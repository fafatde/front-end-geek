const path = require('path')
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    plugins: [
        new VueLoaderPlugin(),
      ],
    mode: 'development',
    entry: './src/js/index',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/i, use: ['style-loader', 'css-loader',
                    // {
                    //     loader: 'postcss-loader',
                    //     plugins:[
                    //         require('autoprefixer')
                    //     ]
                    // }
                ]
            },
            // { test: /\.less$/i, use: ['style-loader', 'css-loader', 'less-loader'] },

            {
                test: /\.(png|jpe?g|gif|svg)$/i, use: [
                    {
                        loader: 'url-loader',
                        options: {
                            outputPath: 'imgs/',
                            publicPath: 'dist/imgs',
                            limit: 2 * 1024
                        }
                    }


                ]
            },
            {
                test: /\.(js|jsx)$/i, use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'
                        ]
                    }
                }]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },

}