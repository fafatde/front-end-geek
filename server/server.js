// 现学的node.js。。
//
// 整体思路是：
// 1.在index.js 创建Vue实例，挂载到某个节点。
// 2.请求接口时，使用webpack编译输出bundle.js,并返回bundle.js的内容。
// 3.接口调用方把接收到的js添加到script标签里
//
// 勉强实现“在线预览”，但不知道这种方式对不对？？
//

const http = require('http');
const fs = require('fs');
var child = require('child_process');

http.createServer((req, res) => {

    console.log('收到请求' + req.url);

    if(req.url !== '/webpack'){
        res.end('404');
        return
    }

    let data = [];
    req.on('data', chunk => {
        data.push(chunk);  // 将接收到的数据暂时保存起来
    })
    req.on('end', () => {
        console.log(JSON.parse(data));
        let code = JSON.parse(data)['code'];

        // 1.获取req的数据，保存Template.vue文件 暂不考虑文件名唯一、重名
        fs.writeFileSync('./src/views/Template.vue', code);
    })

    // 2.执行webpack命令
    child.exec('webpack', function (err, sto) {
        if (err) {
            console.log('err', err);
            return;
        }

        console.log('命令执行成功');
        // console.log(sto);

        // 3.读取bundle.js
        let data = fs.readFileSync('./dist/bundle.js');
        // console.log(data.toString())

        res.writeHeader(200, {
            // 'Access-Control-Allow-Credentials': true, //允许后端发送cookie
            'Access-Control-Allow-Origin': req.headers.origin || '*', //任意域名都可以访问,或者基于我请求头里面的域
            'Access-Control-Allow-Headers': 'X-Requested-With,Content-Type', //设置请求头格式和类型
            'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE,OPTIONS',//允许支持的请求方式
            'Content-Type': 'text/plain; charset=utf-8'//默认与允许的文本格式json和编码格式
        });

        // 4.返回数据
        res.end(data.toString());
    })


}).listen(9998, () => {
    console.log('http://127.0.0.1:9998')
});