const microApps = [
    {
        name: 'subVue',
        entry: '//localhost:9001',
        activeRule: '/sub-vue',
    },
];

const apps = microApps.map(item=>{
    return {
        ...item,
        container: '#container',
        props:{
            routerBase:item.activeRule
        }
    }
})

export default apps

