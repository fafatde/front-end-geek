import uuid from 'uuid/v4'; // 首选。6bit 标记版本, 122bit 随机数 每秒生成 10 亿个，大约需要 85 年才有重复的可能，所以在正常应用情形下这种碰撞概率可以忽略
export default class Vnode {
    constructor(tag, attr, children, parent, childrenTemplate) {
        this.tag = tag;
        this.attr = attr;
        this.children = children;
        this.parent = parent;
        this.childrenTemplate = childrenTemplate;
        this.uuid = uuid();
    }
}
